package stagingQA3;

import org.testng.annotations.Test;
import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import page.OpenStreetMapLocatorPage;

public class TC1_To_verify_page_load_on_browser extends BrowserStackTestNGTest
{
	ExtentTest test1 ;
	Boolean geoIP;
	@BeforeMethod
	public void handleTestMethodName(Method method)
	{
		testName = method.getName();
	}
	@Test
	public void verify_page_load_on_browser() throws InterruptedException, IOException
	{
		System.out.println("QA 1 : To Verify page loads on browser.");

		System.out.println("Verification Steps: The page should load, "
				+ "if GeoIP detection on, should attemp to determine location, "
				+ "if geoIP off, then request address information.");
		test1 = extent.startTest("QA 1: Page Load, GeoIP and Cursor position", 
				"To verify page loads on browser.");

		test1.log(LogStatus.INFO,
				"Test Step: 1. Open Locator in Bowser.");
		test1.log(LogStatus.INFO,
				"Verification Steps :The page should load, if GeoIP detection on, " + "should attemp to determine location, "
						+ "if geoIP off, then request address information."
						+ "2. By default the cursor position should be in the search text box.");
		for (String stagingURL:OpenStreetMapLocatorPage.stagingQA3) {
			try {
			
				test1.log(LogStatus.INFO, "Staging Url :"+stagingURL);

				System.out.println("Staging URL :"+stagingURL);

				//Initializing WebElement from Page Factory.

				OpenStreetMapLocatorPage onPage = PageFactory.initElements(driver, OpenStreetMapLocatorPage.class);

				//Getting aut url.
				
				driver.get(stagingURL);
				
				try {
					wc.until(ExpectedConditions.elementToBeClickable(By.linkText("Close")));
	                onPage.closeButton.click();
	                geoIP=true;
				} catch (Exception e) {
					geoIP=false;
				}


				System.out.println("TC 1 : Check page loaded on browser.");

				test1.log(LogStatus.INFO, "TC 1 : Check page loaded on browser.");
				
				Thread.sleep(3000);

				if (driver.getTitle().contains("Problem in loading page.")) {
					System.out.println("Fail.");

					test1.log(LogStatus.FAIL,
							"Failure ScrrenShots:" + test1.addScreenCapture(captureScreenMethod(dest)));

				} else {

					System.out.println("Pass");

					test1.log(LogStatus.PASS, "Pass.");

				}

				//TC 1.2

				System.out.println("TC 1.2 :Validate if GEoIP is 'ON' , location is detected.");
				test1.log(LogStatus.INFO, "TC 1.2 :Validate if GEoIP is 'ON' , location is detected.");

				if (geoIP) {
					System.out.println("Pass.");
					test1.log(LogStatus.PASS, "GeoIp is ON.");

				} else {

					System.out.println("Pass : GeoIP is off.");
					test1.log(LogStatus.PASS, "GeoIP is off.");

				}

				extent.endTest(test1);

			} catch (Exception e) {
				System.out.println("Exception Occured.");
				e.printStackTrace();
				
			} 
		}

	}

	@AfterMethod
	public void getResult(ITestResult result)
	{
		System.out.println("TC 1 executed.");

		System.out.println("------------------------------------------------------------------------------------");

		driver.quit();

	}

}
