package stagingQA1;

import org.testng.annotations.Test;
import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import static org.testng.Assert.*;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import page.OpenStreetMapLocatorPage;

public class TC10_valid_cityName_invalid_state extends BrowserStackTestNGTest
{
	ExtentTest test10 ;
	@BeforeMethod
	public void handleTestMethodName(Method method)
	{
		testName = method.getName(); 
	}
	@Test
	public void valid_cityName_invalid_state() throws InterruptedException, IOException
	{
		System.out.println(
				"Test Objective :To verify whether the appropriate error message is displayed or not, if we give valid city name and invalid state name.");

		System.out.println(
				"Verification : 1. System should attempt based on GeoIP to determine appropraite valid city "
						+ "or suggest a list of multiple cities that match the input for user to select. "
						+ "2. If system is unable to determine city with invalid state/province provided or left off, An error message"
						+ " 'No locations were found using your search criteria. Please try another input address to search for locations.' should be displayed.");
		test10 = extent.startTest("Valid city name and invalid or not state/province provided.",
				"To verify whether the appropriate error message is displayed or not, if we give valid city name and invalid state name.");


		test10.log(LogStatus.INFO, "Test Step :1. Open locator in the browser."+ 
				"2. Enter a valid city name and invalid state/province 'San Dimas, NONAME'"
				+ " name in the search textbox and press Search Maps button or no state name provided");	

		test10.log(LogStatus.INFO,
				"1. System should attempt based on GeoIP to determine appropraite valid city "
						+ "or suggest a list of multiple cities that match the input for user to select. "
						+ "2. If system is unable to determine city with invalid state/province provided or left off, An error message"
						+ " 'No locations were found using your search criteria. Please try another input address to search for locations.' should be displayed.");
		for (String stagingURL:OpenStreetMapLocatorPage.stagingQA1) {

			try {


				test10.log(LogStatus.INFO, "Staging Url :"+stagingURL);

				System.out.println("Staging URL :"+stagingURL);
				//Initializing WebElemrnt from Page Factory.
				OpenStreetMapLocatorPage onPage = PageFactory.initElements(driver, OpenStreetMapLocatorPage.class);


				//Getting aut url.
				driver.get(stagingURL);


				try {
					wc.until(ExpectedConditions.elementToBeClickable(By.linkText("Close")));
					driver.findElement(By.linkText("Close")).click();

				} catch (Exception e) {
					e.printStackTrace();
				}

				WebElement addtxt = onPage.addressSearch;

				//Test Cases 

				System.out.println("TC 10.1 :Check system suggest a list of multiple cities that match the input for user to select.");
				test10.log(LogStatus.INFO, "TC 10.1 :Check system suggest a list of multiple cities that match the input for user to select.");

				addtxt.sendKeys("San Dimas, NONAME");

				Thread.sleep(2000);
				onPage.buttonSearch.click();


				try {
					wc.until(ExpectedConditions.elementToBeClickable(onPage.closeButton));
					System.out.println("Pass.");
					test10.log(LogStatus.PASS, "Pass.");
				} catch (Exception e1) {
					e1.printStackTrace();

				}
				

//
//				try {
//					assertEquals(driver.findElement(By.xpath("//h3")).getText(), "Please select your location");
//					System.out.println("Pass.");
//					test10.log(LogStatus.PASS, "Pass.");
//
//				} catch (AssertionError e) {
//					System.out.println("Fail.");
//					test10.log(LogStatus.FAIL, "Fail." +e.getMessage()
//					+ test10.addScreenCapture(captureScreenMethod(dest)));	
//
//				}


			} catch (Exception e) {
				e.printStackTrace();
				test10.log(LogStatus.UNKNOWN, "Exception Occured. : " + e.getMessage() + " "
						+ test10.addScreenCapture(captureScreenMethod(dest)));
			} 
		}
	}
	@AfterMethod
	public void getResult(ITestResult result)
	{
		System.out.println("TC 10 executed.");

		System.out.println("------------------------------------------------------------------------------------");

		extent.endTest(test10);


	}
}
