package stagingQA1;

import org.testng.annotations.Test;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import browserStackTestNG.BrowserStackTestNGTest;
import page.OpenStreetMapLocatorPage;

public class TC11_Valid_StreetAddress_City_and_StateProvince_provided extends BrowserStackTestNGTest
{
	ExtentTest test11 ;
	@BeforeMethod
	public void handleTestMethodName(Method method)
	{
		testName = method.getName(); 
	}
	@Test
	public void valid_cityName_invalid_state() throws InterruptedException, IOException
	{
		System.out.println(
				"Test Objective :To verify whether the correct search results are shown or not, "
						+ "when valid Street Address, City and State name is given into the search textbox.");

		System.out.println(
				"Verification :The search results should be displayed for that area entered.");
		test11 = extent.startTest("Valid Street Address, City and State/Province provided.",
				"To verify whether the correct search results are shown or not, "
						+ "when valid Street Address, City and State name is given into the search textbox.");

		test11.log(LogStatus.INFO, "Test Step :1. Open locator in the browser."+ 
				"2. Enter a valid address with street, city and stateor province "
				+ "(like '1442 Paseo Maravilla San Dimas, CA') in the search textbox "
				+ "and press Search button.");

		test11.log(LogStatus.INFO, "Verification Step : 2. The search results should be displayed for that area entered.");
		for (String stagingURL:OpenStreetMapLocatorPage.stagingQA1) {

			try {

				test11.log(LogStatus.INFO, "Staging Url :"+stagingURL);

				System.out.println("Staging URL :"+stagingURL);

				//Initializing WebElemrnt from Page Factory.
				OpenStreetMapLocatorPage onPage = PageFactory.initElements(driver, OpenStreetMapLocatorPage.class);


				//Getting aut url.
				driver.get(stagingURL);

				try {
					wc.until(ExpectedConditions.elementToBeClickable(By.linkText("Close")));
					onPage.closeButton.click();

				} catch (Exception e) {
					e.printStackTrace();
				}
				WebElement addtxt = onPage.addressSearch;


				//Test Cases 

				System.out.println("TC 11.1 :Check search results should be displayed for that area entered.");

				test11.log(LogStatus.INFO, "TC 11.1 :Check search results should be displayed for that area entered.");

				addtxt.sendKeys("1442 Paseo Maravilla San Dimas, CA");

				Thread.sleep(2000);
				onPage.buttonSearch.click();

				try {
					wc.until(ExpectedConditions.presenceOfAllElementsLocatedBy((By.cssSelector("h3"))));
				} catch (Exception e) {
					e.printStackTrace();
					test11.log(LogStatus.UNKNOWN, "Exception Occured." 
							+ test11.addScreenCapture(captureScreenMethod(dest)));	
					System.out.println("Fail.");
				}
				List<WebElement> result=driver.findElements(By.xpath("//*[text()[contains(.,'Anaheim, CA')]]"));

				System.out.println("Total Result Founded : "+result.size());
				if(result.size()>0)
				{
					System.out.println("Pass.");
					test11.log(LogStatus.PASS, "Pass.");

				}
				else
				{
					System.out.println("Fail.");
					test11.log(LogStatus.FAIL, "Fail." 
							+ test11.addScreenCapture(captureScreenMethod(dest)));	

				}



			} catch (Exception e) {
				e.printStackTrace();
				test11.log(LogStatus.FAIL, "Exception Occured. : " + e.getMessage() + " "
						+ test11.addScreenCapture(captureScreenMethod(dest)));
			} 
		}
	}
	@AfterMethod
	public void getResult(ITestResult result)
	{
		System.out.println("TC 11 executed.");

		System.out.println("------------------------------------------------------------------------------------");

		extent.endTest(test11);

	}
}
