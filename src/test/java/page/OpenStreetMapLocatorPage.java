package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import browserStackTestNG.BrowserStackTestNGTest;

public class OpenStreetMapLocatorPage extends BrowserStackTestNGTest
{

	

	//Staging Project URL.
	public static  String zeusUrl[]={"http://zeus.dev.where2getit.com/yong_test/backbone.QA1.html",
			"http://zeus.dev.where2getit.com/yong_test/backbone.QA2.html",
	"http://zeus.dev.where2getit.com/yong_test/backbone.QA3.html"};
	public static String zeusQA1[]={"http://zeus.dev.where2getit.com/yong_test/backbone.QA1.html"};
	public static String zeusQA2[]={"http://zeus.dev.where2getit.com/yong_test/backbone.QA2.html"};
	public static String zeusQA3[]={"http://zeus.dev.where2getit.com/yong_test/backbone.QA3.html"};
	
	public static String stagingQA1[]={"http://hosted.where2stageit.com/yongtest/backbone.QA1.html"};
	public static String stagingQA2[]={"http://hosted.where2stageit.com/yongtest/backbone.QA2.html"};
	public static String stagingQA3[]={"http://hosted.where2stageit.com/yongtest/backbone.QA3.html"};

	//Address Search Input TextBox
	@FindBy(name="addressline")
	public 
	WebElement addressSearch;

	//Search Address Drop Down box.
	@FindBy(id="search_country")
	public 
	WebElement searchCountryOption;

	//Search Address Drop Down box.
	@FindBy(name="searchradius")
	public 
	WebElement chooseRadius;

	//Pop Up Close button.
	@FindBy(className="closeBtn")
	public WebElement closeButton;

	//Button Search
	@FindBy(className="button-search")
	public WebElement buttonSearch;

	//Modal Message Box
	@FindBy(xpath="//*[@class='modal']/p")
	public WebElement modalmsg;







}
